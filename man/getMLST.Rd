% Generated by roxygen2: do not edit by hand
% Please edit documentation in R/getMLST.R
\name{getMLST}
\alias{getMLST}
\title{getMLST   download from http://pubmlst.org/data/dbases.xml the MLST genes and mlst_definitions txt file}
\usage{
getMLST(xml, species, db_nt, rm_old, ...)
}
\arguments{
\item{xml}{= http://pubmlst.org/data/dbases.xml # PubMLST database}

\item{species}{name of the Species (es: "Staphylococcus aureus")}

\item{db_nt}{folder where getMLST download the files}

\item{rm_old}{remove all files in db_nt}
}
\description{
getMLST   download from http://pubmlst.org/data/dbases.xml the MLST genes and mlst_definitions txt file
}
\author{
Mattia Dalsass, Alessandro Brozzi
}
\keyword{getMLST}

