#!/bin/bash

# View BAM file, take each read from the 10th column and report its length,
# then chose the longest one.
# samtools view mapped.bam  : View alignment file.
# awk '{print length($10)}' : Teport the length of the 10th column (the read
#                             sequence).
# sort -rnu                 : Take unique values as numeric and sort them in
#                             decreasing order.
# head -1                   : Keep only the first line (the highest read
#                             length).
#
# In the case of trimmed reads, the length of the longest one is taken as a
# reference.
#
maxReadLength="$(samtools view mapped.bam | awk '{print length($10)}' | sort -rnu | head -1)"

# Test whether there are reads mapped to the considered locus.
# When there are no reads, $maxReadLength is empty.
if [ -z "$maxReadLength" ]; then
    echo "Warning: No read mapped to this locus in file 'mapped.bam' in directory '"`pwd`"' !"
    exit 1
fi

# Foreach given percentage, compute the corresponding length and generate
# a file containnig the reads matching the reference sequence with more
# nucleotides than this length threshold.
for percentage in 30 60 90
do
    # Compute the length corresponding to the given percentage.
    let threshold=($maxReadLength*$percentage)/100

    # Extract reads matching the reference sequence with more than
    # "threshold" nucleotides.
    samtools view -h mapped.bam \
        | perl -lane '$l = 0; $F[5] =~ s/(\d+)[MX=DN]/$l+=$1/eg; print if $l > '$threshold' or /^@/' \
        | samtools view -bS - > bar$percentage.bam
done

