#' createDB read the .tfa or .fasta files in database folder (db_nt) and create a list of files 
#' 
#' @param db_nt (folder with fasta sequences database and mlst_definitions txt file)
#'          
#' @author Mattia Dalsass (mattia.dalsass@gmail.com), Alessandro Brozzi 
#'         
#' @keywords createDB
#' 
#' @export
createDB <- function(db_nt){
  
  ########################################
  ####  List the .tfa or .fasta files ####
  ########################################
  
  summary     <- list()
  tfa_files   <- list.files(db_nt, pattern = "\\.tfa$")
  fasta_files <- list.files(db_nt, pattern = "\\.fasta$")
  
  if (length(tfa_files)>0){
    genes <- list.files(db_nt, full.names=F,pattern="\\.tfa$")
    genes_path <- list.files(db_nt, full.names=T,pattern="\\.tfa$")
  }
  if (length(fasta_files)>0){
    genes <- list.files(db_nt, full.names=F,pattern="\\.fasta$")
    genes_path <- list.files(db_nt, full.names=T,pattern="\\.fasta$")
  }
  
  #################################################
  ####  Create a list of files with full path  ####
  #################################################
  
  for (i in 1:length(genes)){
    summary[["db_gene"]][i]   <- paste(db_nt,genes[i],sep="")
    summary[["only_prot"]][i] <- FALSE
    
    cmd <- paste0("perl -pi -w -e 's/-/_/g;' ",genes_path[i])
    system(cmd)
    cmd <- paste0("perl -pi -w -e 's/__/_/g;' ",genes_path[i])
    system(cmd)
    cmd <- paste0("perl -pi -w -e 's/\ /_/g;' ",genes_path[i])
    system(cmd)
    cmd <- paste0("perl -i -pe 's|[ \t]||g' ",genes_path[i])
    system(cmd)
    cmd <- paste0("perl -pi -w -e 's/(?!>)[[:punct:]]/_/g;' ",genes_path[i])
    system(cmd)
    makeblastdb(genes_path[i],seqtype="nucl")
  }
  summary[["db_N_path"]]      <- db_nt
  return(summary)
  
}


