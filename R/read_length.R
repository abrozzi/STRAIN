#' read_length calculate reads length in fastq.gz file
#' 
#' @param    sample sample path directory in fastq.gz format
#'          
#' @author   Mattia Dalsass (mattia.dalsass@gmail.com), Alessandro Brozzi 
#'         
#' @keywords read_leangth
#' 
#' @export
read_length <- function(sample){
  
  lf       <- list.files(sample,full=TRUE, pattern="fastq.gz")
  x        <- lf[1]
  read_len <- system(paste("gunzip -c ", x," | awk 'NR%4 == 2 {lengths[length($0)]++} END {for (l in lengths) {print l}}' ",sep=""),intern=TRUE)
  return(read_len)
  
}
