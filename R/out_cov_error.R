#' out_cov_error if the coverage for one or more gene is <10 and SPAdes return an error the function return all the sample ST = NA  
#' 
#' @param MFT (MasterFileList output)
#' 
#' @param db_nt folder with fasta sequences database and mlst_definitions txt file
#' 
#' @param species the name of the Species (es: "Staphylococcus aureus")
#'  
#' @param dir_tree List of directory path
#' 
#' @author Mattia Dalsass (mattia.dalsass@gmail.com), Alessandro Brozzi 
#'         
#' @keywords out_cov_error
#' 
#' @export  
out_cov_error <- function(MFT,
                          db_nt,
                          species,
                          dir_tree){
  cat("Coverage to low for MLST identification")
  ################################################
  ####   READ MLST TABLE PROFILE FROM db_nt   ####
  ################################################
  
  spec=species
  profile <- list.files(db_nt,pattern =paste0("profile_",spec,".txt"),full.names = T)[1]
  profile <- read.table(profile,header=T,sep="\t",fill=T)
  
  if (sum(colnames(profile)=="clonal_complex")==0){profile[,"clonal_complex"] <- NA}
  
  profile_temp <- profile[,sort(colnames(profile)[2:(ncol(profile)-1)])]
  profile_ST   <- cbind(profile[,1],profile_temp,profile[,ncol(profile)])
  
  colnames(profile_ST)[1] <- "ST"
  colnames(profile_ST)[ncol(profile)] <- "clonal_complex"
  
  cat("Read ST profile","\n")
  
  ###################################
  ####    Define res_CC matrix   ####
  ###################################
  
  res_CC       <- matrix(NA,1,length(c("sample", colnames(profile_ST))))
  colnames(res_CC) <- c("sample", colnames(profile_ST))
  res_CC[,1]   <- MFT$sample.name
  res_CC[,2]   <- "low_coverage"
  match_col=which(colnames(res_CC)=="clonal_complex")
  res_CC[,3:match_col]  <- "low_coverage"
  res_CC       <- as.data.frame(res_CC, stringsAsFactors=FALSE)
  
  
  
  #########################################################################
  ####   Write matrix output to a RESULTS_Clonal_Complex_...txt file   ####
  #########################################################################
  spec=paste(strsplit(species, "\\s+")[[1]],collapse = "_")
  file      <- paste(dir_tree$result,paste("RESULTS_Clonal_Complex_",spec,".txt",sep=""),sep="")
  append    <- ifelse(file.exists((file)), TRUE, FALSE)
  col.names <- ifelse(file.exists((file)), FALSE, TRUE)
  
  if (append==T){
    
    file_r  <- as.data.frame(read.delim(file=file,sep = "\t"))
    l_match <- row.match(res_CC,file_r,nomatch = NA)
    
    if (is.na(l_match)==T){
      
      write.table(res_CC,
                  file=file,
                  append=append,
                  sep="\t",
                  row.names = F,
                  col.names=col.names,
                  quote=F)
    }
    
    else{
      
      break
      
    }
  } 
  
  else{
    
    write.table(res_CC,
                file=file,
                append=append,
                sep="\t",
                row.names = F,
                col.names=col.names,
                quote=F)
    
  }
  cat("Low_coverage_write_to_file")
} 
