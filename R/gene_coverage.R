#' gene_coverage calculate coverage from bam file with .tfa or .fasta file reference
#' 
#' @param  bamfile file .bam 
#' 
#' @param  dat_N directory with .tfa file
#'    
#' @author Mattia Dalsass (mattia.dalsass@gmail.com), Alessandro Brozzi 
#'         
#' @keywords gene_coverage
#' 
#' @export 
gene_coverage <- function(bamfile,
                          dat_N){
  require("Rsamtools",quietly = T)
  require("seqinr",quietly = T)
  
  ###############################################################
  #### Read the bam file and calculate num_read and read_len #### 
  ###############################################################
  
  mapped       <- scanBam(bamfile)
  num_read     <- length(mapped[[1]]$qname)
  read_len     <- unique(mapped[[1]]$qwidth)
  sam_loc_name <- strsplit(as.character(mapped[[1]]$rname),"_")[[1]][1]  
 
  ###################################
  #### Read the fasta reference  ####
  ###################################
  
  tfa_files   <- list.files(dat_N, pattern = "\\.tfa$")
  fasta_files <- list.files(dat_N, pattern = "\\.fasta$")
  if (length(tfa_files)>0){
    gene_type <- list.files(dat_N,pattern = "\\.tfa$",full.names = T)
  }
  if (length(fasta_files)>0){
    gene_type <- list.files(dat_N,pattern = "\\.fasta$",full.names = T)
  }
  
  ########################################################
  ####      Calculate the length of reference fasta   ####
  ########################################################
  
  locus_n <- list()
  for (i in 1:length(gene_type)){
    len  <- 0
    name <- strsplit(gene_type,"/")
    name_short  <- lapply(name, function(x) x[length(x)])
    locus_names <- unlist(strsplit(unlist(name_short),".tfa"))
    locus <- read.fasta(gene_type[i])
    for (j in 1:length(locus)){
      len   <- (len+length(locus[[j]]))
      len_F <- len/length(locus)
    }
    locus_n[locus_names[i]] <- round(len_F)
  }
  var_len  <- locus_n[which(names(locus_n)==sam_loc_name)]
  
  ############################
  #### Calculate coverage ####
  ############################
  
  coverage <- NULL
  coverage <- round((read_len*num_read)/as.numeric(var_len))
  names(coverage) <- sam_loc_name
  return(coverage)
}


