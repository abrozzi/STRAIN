-----------
# STRAIN

-----------
### STRAIN: an R package for multi-locus sequence typing from whole genome sequencing data

-----------
### Contents

* [Dependencies](#dependencies)

* [Install](#install)

* [Clone](#clone)

* [Current release](#current-release)

* [Tutorial: Basic usage for Multi Locus Sequence Typing](#tutorial-basic-usage-for-multi-locus-sequence-typing)

* [Tutorial: Basic usage for Single Gene Variant Identification](#tutorial-basic-usage-for-single-gene-variant-identification)

* [Tutorial: Identification of species with 16s database](#tutorial-identification-of-species-with-16s-database)

* [Git Command line instructions](#git-command-line-instructions)

-----------
# Dependencies


(set the programs in PATH)

- SAMtools      (v1.2 or higher)                 https://sourceforge.net/projects/samtools/files/samtools/

- bedtools2     (v2)		                   http://bedtools.readthedocs.org/en/latest/content/installation.html

- bowtie2       (v2.2.6 or higher, v2.2.9 or higher for multiple threads option)                     http://sourceforge.net/projects/bowtie-bio/files/bowtie2/

- SPAdes        (v3.6 or higher)             http://bioinf.spbau.ru/spades

- Velvet        (v1.2.10)                  https://www.ebi.ac.uk/~zerbino/velvet/

- Blast +       (v2.3.0 or higher)       ftp://ftp.ncbi.nlm.nih.gov/blast/executables/blast+/LATEST/

- R             (v3.2.3 or higher)                 https://www.r-project.org/

R package Dependencies:

- seqinr        (v3.1-3  or higher)

- xml2 (xml2_1.1.1) (install.packages("xml2"))

- RCurl (RCurl_1.95-4.10)

-----------

# Install

devtools::install_git("https://gitlab.com/abrozzi/STRAIN.git")

-----------

# Docker

   docker pull fpetrogalli/strain:base
   
   docker run -it fpetrogalli/strain:base
   
   su - strain
   
-----------

# Clone


git clone https://gitlab.com/abrozzi/STRAIN.git

-----------

# Current release
# v0.1.0

-----------

# Tutorial: Basic usage for Multi Locus Sequence Typing

##### Download the ERR024691 fastq.gz files from http://www.ebi.ac.uk/ena and save it in a directory with a unique name

Es: (~/ERR024691)

-----------

##### Download with the getMLST function the pubMLST definition and the genes fasta files for a specific bacterial species:

```
Set the proxy with:

Sys.setenv(http_proxy="http://.....:0000")

Sys.setenv(https_proxy="https://.....:0000")

Sys.setenv(ftp_proxy="ftp://.....:0000")

Usage:

require(STRAIN)
require(seqinr)
require(xml2)
require(RCurl)

Arguments:

xml="http://pubmlst.org/data/dbases.xml"

species="Staphylococcus aureus"             #(es. of species names are present in variable "Species")

db_nt="~/database_gene_PUBMLST/"            #(local directory where getMLST download the fasta files and MLST definition txt file)

Function:

getMLST(xml,species,db_nt)



```

-----------

##### Run the STRAIN pipeline with the run_STRAIN funtion

```

Usage:

Arguments:

sample_folder=      # Local folder with fastq or filt...fastq files es: ("~/ERR024691") (by default STRAIN takes filt...fastq file if is present)

db_nt=              # folder with variants fasta files ("~/database_gene_PUBMLST/")

result_folder=      # Local folder for STRAIN results

ncore=              # Number of cores (default max(1, detectCores()-1))

threads_bowtie=     # Number of threads for bowtie mapping (default 8)

species_name=       # Species name (es: "Staphylococcus aureus")

bowtie_build=       # build the bowtie2 index TRUE/FALSE, default TRUE (if you have a local index in a directory set bowtie_build=FALSE)

MLST=               # "YES" for MLST or "NO" for Single Gene Variant Identification (default "YES")

Function:

require(seqinr)
require(STRAIN)
require(parallel)
require(tools)

run_STRAIN(sample_folder , db_nt , result_folder , ncore=max(1, detectCores()-1), threads_bowtie=8, species_name, bowtie_build=TRUE, MLST="YES") 


```

##### The final result is present in "result_folder"/Results_STRAIN/


-----------

# Tutorial: Basic usage for Single Gene Variant Identification

Gene Variant Identification
If in "db_nt" database is present only one .fasta or .tfa file, STRAIN return the gene variant without MLST definition.

```

Usage:

Arguments:

sample_folder=      # Local folder with fastq or filt...fastq files es: ("~/ERR024691") (by default STRAIN takes filt...fastq file if is present)

db_nt=              # folder with variants fasta files ("~/database_gene_PUBMLST/")

result_folder=      # Local folder for STRAIN results

ncore=              # Number of cores (default max(1, detectCores()-1))

threads_bowtie=     # Number of threads for bowtie mapping (default 8)

species_name=       # Species name (es: "Staphylococcus aureus")

bowtie_build=       # build the bowtie2 index TRUE/FALSE, default TRUE (if you have a local index in a directory set bowtie_build=FALSE)

MLST=               # "YES" for MLST or "NO" for Single Gene Variant Identification (default "YES")

Function:

require(seqinr)
require(STRAIN)
require(parallel)
require(tools)

run_STRAIN(sample_folder , db_nt , result_folder , ncore=1, threads_bowtie=8, species_name, bowtie_build=TRUE, MLST="NO") 


```

-----------

# Tutorial: Identification of species with 16s database

Put the 16s.fasta file in the "db_nt" directory.

Run STRAIN:

- Set bowtie_build=TRUE if you want a new bowtie2 index (very time consuming with a big fasta files)
- Set bowtie_build=FALSE if you have a local bowtie2 index

```

Usage:

Arguments:

sample_folder=      # Local folder with fastq or filt...fastq files es: ("~/ERR024691") (by default STRAIN takes filt...fastq file if is present)

db_nt=              # folder with 16s fasta file/files ("~/database_gene_PUBMLST/")

result_folder=      # Local folder for STRAIN results

ncore=              # Number of cores (default max(1, detectCores()-1))

threads_bowtie=     # Number of threads for bowtie mapping (default 8)

species_name=       # Species name (es: "Staphylococcus aureus")

bowtie_build=       # build the bowtie2 index TRUE/FALSE, default TRUE (if you have a local index in a directory set bowtie_build=FALSE)

spec_ident=         # "YES" for 16s identification

Function:

require(seqinr)
require(STRAIN)
require(parallel)
require(tools)

run_STRAIN(sample_folder , db_nt , result_folder , ncore=1, threads_bowtie=8, species_name, bowtie_build=TRUE, spec_ident="YES") 


```

-----------

# Git Command line instructions

### Git global setup

```

git config --global user.name "username"
git config --global user.email "email"


```

### Create a new repository in gitlab (Example)

```

# Create a project in gitlab, es: PIPPO
git clone https://gitlab.com/DalsaMat/PIPPO.git
cd PIPPO
# Create README file
touch README.md
# Create package structure
create package directories (R,man,ecc..) and add functions, scripts ecc..
# Commit changes
git add .
git commit -m "description"
git push -u origin master

```

### Existing folder in local or Git repository (Example)

```

# If you have a R package in PIPPO directory 
cd PIPPO
git init
git remote add origin https://gitlab.com/DalsaMat/PIPPO.git
git add .
git commit
git push -u origin master

```